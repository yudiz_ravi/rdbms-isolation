'use strict'
const Sequelize = require('sequelize')
const { DataTypes, Transaction, literal } = require('sequelize')

const sequelize = new Sequelize('test', 'root', '', {
    host: "localhost",
    dialect: "mysql",
    logging: false,
    define: {
        freezeTableName: false
    },
    pool: {
        max: 50,
        min: 10,
        idle: 10000,
        handleDisconnects: true
    }
});
sequelize.authenticate().then(()=> {
    console.log('DB Connection Successfully!!!')
}).catch(error => {
    console.log('Error While Connecting to DB', error)
})

module.exports = { sequelize, DataTypes, Transaction, literal }
