const { sequelize, Transaction, literal } = require('../config/connection')
const { userdeposits } = require('../models/userdeposits')
require('../config/connection')

// isolation level - Read committed
async function phantomRead() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    const t2 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    try {
        console.log('........ Problem with Phantom Read ........')
        console.log('Transaction 1 Get Started...')
        const data = await userdeposits.findAll({ raw: true }, { transaction: t1 })
        console.log('Initial Data :: ', data)
        const sum1 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`sum of nAmount from t1 : ${sum1}`)

        console.log('Transaction 2 Started...')
        await userdeposits.create({ nAmount: 25, nCount: 1 }, { transaction: t2 })
        console.log('New row added in Transaction 2')

        console.log('Transaction 2 Gets COMMIT')
        await t2.commit()

        console.log('Transaction 1 continues...')
        const sum2 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`sum of nAmount from t1 : ${sum2}`)

        console.log('Transaction 1 Gets COMMIT')
        await t1.commit()
    } catch (error) {
        console.log('Transaction Error', error)
        await t1.rollback()
        await t2.rollback()
    }
}

// Phantom Read with Isolation level - serializable
async function phantomReadWithIsolation() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
    })
    const t2 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    try {
        console.log('........ Phantom Read Solution - SERIALIZABLE ........')
        console.log('Transaction 1 Gets Started...')
        const data = await userdeposits.findAll({ raw: true }, { transaction: t1 })
        console.log('Initial Data :: ', data)

        const sum1 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`sum of nAmount from t1 : ${sum1}`)

        console.log('In between Transaction 2 Started...')
        console.log('Transaction 2 Gets Blocked...')
        await userdeposits.create({ nAmount: 10, nCount: 1 }, { transaction: t2 })
        console.log('New row added in Transaction 2')

        console.log('Transaction 2 Gets COMMIT')
        await t2.commit()

        console.log('Transaction 1 Continues...')
        const sum2 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`sum of nAmount from t1 : ${sum2}`)

        console.log('Transaction 1 Gets COMMIT')
        await t1.commit()
    } catch (error) {
        console.log('Transaction Error', error)
        await t1.rollback()
        await t2.rollback()
    }

}

// phantomRead()
phantomReadWithIsolation()