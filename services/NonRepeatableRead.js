const { sequelize, Transaction, literal } = require('../config/connection')
const { userdeposits } = require('../models/userdeposits')
require('../config/connection')

//non-repeatable reads 

// Isolation level  = Repeatable Read
async function nonRepeatableRead() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED
    })
    const t2 = await sequelize.transaction() // Default to REPEATABLE_READ
    try {
        console.log('........ Non-Repeatable Read Issue ........')
        console.log('Transaction 1 Started...')
        const sum1 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`Sum of nAmount from t1 : ${sum1}`)

        console.log('Transaction 2 Started...')
        const nAmount = 5
        await userdeposits.update({
            nAmount: literal(`nAmount + ${nAmount}`)
        }, { where: { id: 1 }, transaction: t2 })

        console.log('Transaction 2 Gets COMMIT')
        await t2.commit()

        console.log('Transaction 1 Continues...')
        const sum2 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`sum of nAmount from t1 : ${sum2}`)

        console.log('Transaction 1 Gets COMMIT')
        await t1.commit()
    }
    catch (error) {
        await t1.rollback()
        await t2.rollback()
        console.log('Transaction Error', error)
    }

}

// Non Repeatable Read solution with isolation level - Repeatable Read
async function nonRepeatableReadWithIsolation() {
    const t1 = await sequelize.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    })
    const t2 = await sequelize.transaction()
    try {
        console.log('........ Non-Repeatable Read Solution - REPEATABLE_READ ........')
        console.log('Transaction 1 Started...')
        const sum1 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`sum of nAmount from t1 : ${sum1}`)

        console.log('Transaction 2 Started...')
        const nAmount = 5
        await userdeposits.update({
            nAmount: literal(`nAmount + ${nAmount}`)
        }, { where: { id: 1 }, transaction: t2 })

        console.log('Transaction 2 Gets COMMIT')
        await t2.commit()

        console.log('Transaction 1 Continues...')
        const sum2 = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`sum of nAmount from t1 : ${sum2}`)
        console.log('Transaction 1 Gets COMMIT')
        await t1.commit()
    }
    catch (error) {
        await t1.rollback()
        await t2.rollback()
        console.log('Transaction Error', error)
    }

}

nonRepeatableRead()
nonRepeatableReadWithIsolation()
