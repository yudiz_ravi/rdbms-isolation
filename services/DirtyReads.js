const { sequelize, Transaction, literal } = require('../config/connection')
const { userdeposits } = require('../models/userdeposits')
require('../config/connection')
const READ_COMMITTED = Transaction.ISOLATION_LEVELS.READ_COMMITTED
const READ_UNCOMMITTED = Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED

// Problem with Dirty Read Isolation Level - READ Uncommitted
async function dirtyRead() {
    const t1 = await sequelize.transaction({ isolationLevel: READ_UNCOMMITTED })
    const t2 = await sequelize.transaction({ isolationLevel: READ_UNCOMMITTED })
    try {
        console.log('........ Problem with Dirty Read - READ_UNCOMMITTED ........')
        console.log('Transaction 1 Started...')
        const data = await userdeposits.findAll({ raw: true }, { transaction: t1 })
        console.log('Initial Data :: ', data)

        // Transaction 2 started in between of Transaction 1
        console.log('Transaction 2 Started...')
        const nAmount = 5
        await userdeposits.update({
            nAmount: literal(`nAmount - ${nAmount}`)
        }, { where: { id: 1 }, transaction: t2 })

        console.log('Transaction 1 Continues...')
        const sum = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`nAmount Sum : ${sum}`)

        console.log('Transaction 1 Gets COMMIT')
        await t1.commit()
        console.log('Transaction 2 Gets ROLLBACK')
        await t2.rollback()
    } catch (error) {
        t1.rollback()
        t2.rollback()
        console.log('Transaction Error ::', error)
    }
}

// Solution of Dirty Read With Isolation Level - READ Committed
async function dirtyReadSolution() {
    const t1 = await sequelize.transaction({ isolationLevel: READ_COMMITTED })
    const t2 = await sequelize.transaction({ isolationLevel: READ_COMMITTED })
    try {
        console.log('........ Solution of DirtyRead - READ_COMMITTED ........')
        console.log('Transaction 1 Started...')

        const data = await userdeposits.findAll({ raw: true }, { transaction: t1 })
        console.log('Initial Data :: ', data)
        
        console.log('In between Transaction 2 Started...')
        const nAmount = 5
        await userdeposits.update({
            nAmount: literal(`nAmount + ${nAmount}`)
        }, { where: { id: 1 }, transaction: t2 })

        console.log('Transaction 1 Continues...')
        const sum = await userdeposits.sum('nAmount', { transaction: t1 })
        console.log(`nAmount Sum : ${sum}`)

        console.log('Transaction 1 Gets COMMIT')
        await t1.commit()
        console.log('Transaction 2 Gets ROLLBACK')
        await t2.rollback()
    } catch (error) {
        t1.rollback()
        t2.rollback()
        console.log('Transaction Error', error)
    }
}

dirtyRead()
dirtyReadSolution();

