'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userdeposits', {
      id: { type: Sequelize.INTEGER(11), allowNull: false, autoIncrement: true, primaryKey: true },
      nAmount: { type: DataTypes.FLOAT(9, 2), defaultValue: 0 },
      nCount: { type: DataTypes.FLOAT(9, 2), defaultValue: 0 }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userdeposits');
  }
};
