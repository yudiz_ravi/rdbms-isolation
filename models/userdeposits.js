require('../config/connection')
const { sequelize, DataTypes } = require('../config/connection')

const userdeposits = sequelize.define('userdeposits', {
    id: { type: DataTypes.INTEGER(11), allowNull: false, autoIncrement: true, primaryKey: true },
    nAmount: { type: DataTypes.FLOAT(9, 2), defaultValue: 0 },
    nCount: { type: DataTypes.FLOAT(9, 2), defaultValue: 0 }
})

module.exports = { userdeposits }
